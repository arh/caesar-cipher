import tkinter
c=tkinter.Canvas(width=400,height=300)
c.pack()

entry1=tkinter.Entry()
entry1.place(x=250,y=250)
entry2=tkinter.Entry()
entry2.place(x=250,y=275)
c.create_text(220,250,text='text:',tag='instructions',anchor='nw')
c.create_text(220,275,text='key:',tag='instructions',anchor='nw')

def encrypt():
	encrypted=''
	text=entry1.get()
	n=int(entry2.get())
	c.delete('result')
	while n>=26:
		n=n-26
	for i in range(len(text)):
		decimal=ord(text[i])
		if 97<=decimal<=122:
			decimal=decimal+n
			if decimal>122:
				decimal=decimal-26
			encrypted=encrypted+chr(decimal)
		elif 65<=decimal<=90:
			decimal=decimal+n
			if decimal>90:
				decimal=decimal-26
			encrypted=encrypted+chr(decimal)
		else:
			encrypted=encrypted+chr(decimal)
	print(encrypted)
	c.create_text(50,50,text=encrypted,tag='result')

def decrypt():
	decrypted=''
	text=entry1.get()
	n=int(entry2.get())
	c.delete('result')
	while n>=26:
		n=n-26
	for i in range(len(text)):
		decimal=ord(text[i])
		if 97<=decimal<=122:
			decimal=decimal-n
			if decimal<97:
				decimal=decimal+26
			decrypted=decrypted+chr(decimal)
		elif 65<=decimal<=90:
			decimal=decimal-n
			if decimal<65:
				decimal=decimal+26
			decrypted=decrypted+chr(decimal)
		else:
			decrypted=decrypted+chr(decimal)
	print(decrypted)
	c.create_text(50,50,text=decrypted,tag='result')

button1=tkinter.Button(text='encrypt',command=encrypt)
button1.place(x=10,y=240)
button2=tkinter.Button(text='decrypt',command=decrypt)
button2.place(x=10,y=270)

c.mainloop()
